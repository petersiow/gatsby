import React from "react"
// import { useStaticQuery, graphql } from "gatsby"
// import Img from "gatsby-image"

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `useStaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `useStaticQuery`: https://www.gatsbyjs.org/docs/use-static-query/
 */


export default class Calc extends React.Component {
    getPrice = (price) => {
        const usd = 23;
        const fee = 41;
        return ((100 * (10 * price + usd)) / (1000 - fee)).toFixed(2);
    }

    updatePrice = ({target}) => {
        const {value} = target
        this.setState({
            total: this.getPrice(value)
        })
    }
    state = {
        total: 0,

    }

    render() {
        return (
            <div>
                <input type="text" id="price" onKeyUp={this.updatePrice}/>
                <div style={{
                    color: "#155724",
                    backgroundColor: "#d4edda",
                    borderColor: "#c3e6cb",
                    position: "relative",
                    padding: ".75rem 1.25rem",
                    margin: "1rem 0",
                    border: "1px solid transparent",
                    borderRadius: ".25rem"

                }}>
                    Your Price is <span id="final">{this.state.total}</span> HKD;
                </div>
            </div>
        )
    }
}
